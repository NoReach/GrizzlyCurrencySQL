package com.grizzlynetworking.HelpingHassan;

import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

public class ExamplePlugin extends JavaPlugin {

    @Getter private SQLHandler sqlHandler;
    @Getter private ConfigManager configManager;


    @Override
    public void onEnable(){
        this.configManager = new ConfigManager(this);
        this.sqlHandler = new SQLHandler(this);
    }


    @Override
    public void onDisable(){

    }
}
