package com.grizzlynetworking.HelpingHassan;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import java.util.UUID;

public class ExampleListener implements Listener {


    private ExamplePlugin examplePlugin;


    public ExampleListener(ExamplePlugin examplePlugin) {
        this.examplePlugin = examplePlugin;
    }


    @EventHandler
    public void onASyncJoin(AsyncPlayerPreLoginEvent e){
        UUID uuid = e.getUniqueId();
        int currentTokens = this.examplePlugin.getSqlHandler().getPlayerCurrency(uuid);
    }


    @EventHandler
    public void onMine(BlockBreakEvent e){
        Player player = e.getPlayer();
        this.examplePlugin.getSqlHandler().updatePlayer(player.getUniqueId(), 10); //This will add 10 tokens to their balance.
        //If they do not exist in the database then it will create a new entry with their UUID and save 10 tokens.
    }
}
