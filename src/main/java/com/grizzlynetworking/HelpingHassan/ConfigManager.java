package com.grizzlynetworking.HelpingHassan;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.Plugin;

public class ConfigManager {


    private ExamplePlugin plugin; //CHANGE THIS WITH THE OBJECT OF YOUR PLUGIN CLASS

    public ConfigManager(ExamplePlugin plugin) {
        this.plugin = plugin;
        this.init();
    }


    @Getter @Setter private String TABLE_NAME = "";


    private void init()
    {

        this.plugin.getConfig().addDefault("Settings.SQL.TableName", "REALM_ONE_CURRENCY");
        this.plugin.getConfig().options().copyDefaults(true);
        this.plugin.saveConfig();
        this.plugin.reloadConfig();
        this.TABLE_NAME = this.plugin.getConfig().getString("Settings.SQL.TableName");

    }


}
