package com.grizzlynetworking.HelpingHassan;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class SQLHandler {

    private ExamplePlugin plugin; //CHANGE THIS WITH THE OBJECT OF YOUR PLUGIN CLASS


    public SQLHandler(ExamplePlugin plugin) {
        this.plugin = plugin;
        this.init();
    }

    @Getter private Connection connection; //SQL Connection Object
    @Getter private String IP;
    @Getter private String DATABASE;
    @Getter private String USERNAME;
    @Getter private String PASSWORD;


    /**
     * This is the init method, this will initialise the connection with the SQL Server and create the required tables if needed
     */
    private void init(){
        try{
            this.connection = DriverManager.getConnection("jdbc:mysql://" + this.getIP() + "/" + this.getDATABASE() + "/", this.getUSERNAME(), this.getPASSWORD());
            Statement TABLE_STATEMENT = this.connection.createStatement();
            TABLE_STATEMENT.executeUpdate("CREATE TABLE IF NOT EXISTS `" + this.plugin.getConfigManager().getTABLE_NAME() + "` `uuid` VARCHAR(60), `currency` INT NOT NULL DEFAULT '0';");
        }catch (SQLException e){
            Bukkit.getLogger().info("[PluginName] >> An SQL Error occured whilst initialising the connection.....");
        }
    }


    /**
     * This will create a record in the SQL Table for the user with a default currency amount of 0.
     * Will run all of the below code as "synchronized"
     * @param uuid The uuid of the player that you'd like to create
     */
    public synchronized void createPlayerData(UUID uuid){
        try{
            PreparedStatement statement = this.connection.prepareStatement("INSERT INTO `" + this.plugin.getConfigManager().getTABLE_NAME() + "` (uuid, currency) VALUES (?, ?)");
            statement.setString(1, uuid.toString());
            statement.setInt(2, 0);
            statement.executeUpdate();
            statement.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }


    /**
     * This method will get the players currency if exists. It will run this all ASync to minimise server impact.
     * @param uuid The player you'd like to get the amount of currency for
     * @return Will return an int if it has found the player in the database
     */
    public int getPlayerCurrency(UUID uuid) {
        AtomicInteger value = new AtomicInteger(-1);
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = connection.prepareStatement("SELECT * FROM `" + plugin.getConfigManager().getTABLE_NAME() + "` WHERE `uuid` = ?;");
                    statement.executeQuery();
                    ResultSet results = statement.getResultSet();
                    if (results.next()){
                        value.set(results.getInt("currency"));
                    }else{
                        createPlayerData(uuid);
                        value.set(0);
                    }
                    results.close();
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }.runTaskAsynchronously(this.plugin);
        return value.get();
    }


    /**
     * Method to update a players data in the database, if they do not exist, create them, then update it.
     * Will run all the code as "synchronized"
     * @param uuid The UUID of the player you'd like to update
     * @param amountToAdd The amount of currency you'd like to add to the player
     */
    public synchronized void updatePlayer(UUID uuid, int amountToAdd){
        try{
            int currentAmount = getPlayerCurrency(uuid);
            try {
                PreparedStatement statement = this.connection.prepareStatement("UPDATE `" + this.plugin.getConfigManager().getTABLE_NAME() + "` SET `currency` = ? WHERE `uuid` = ?;");
                statement.setInt(1, (currentAmount + amountToAdd));
                statement.setString(2, uuid.toString());
                statement.executeUpdate();
                statement.close();
            }catch (SQLException e){
                e.printStackTrace();
            }
        }catch (NullPointerException e){
            createPlayerData(uuid);
            updatePlayer(uuid, amountToAdd);
        }
    }



}
